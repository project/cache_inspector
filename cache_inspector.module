<?php

/**
 * @file
 * A module to inspect the serialzied values in the drupal cache tables.
 */

/**
 * Cache inspector.
 *
 * @mainpage
 *
 * @section project_page Project page
 * - @link http://drupal.org/project/cache_inspector Drupal project page "Cache inspector" (cache_inspector) @endlink
 *
 * @section git_repository Git repository
 * - @link http://drupalcode.org/project/cache_inspector.git Drupal git repository @endlink
 */

/**
 * @defgroup drupal_hooks Drupal hooks
 * @{
 * Hooks used by the module.
 */

/**
 * Implements hook_menu().
 */
function cache_inspector_menu() {
  $items = array();

  $items['admin/config/development/cache-inspector'] = array(
    'title' => 'Cache inspector',
    'description' => 'To watch whats going on in the drupal cache tables.',
    'page callback' => 'cache_inspector_view',
    'access arguments' => array('access cache inspector'),
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function cache_inspector_theme($existing, $type, $theme, $path) {
  return array(
    'cache_inspector_data' => array(
      'variables' => array(
        'cid' => NULL,
        'data' => NULL,
      ),
    ),
    'cache_inspector_page' => array(
      'variables' => array(
        'form' => NULL,
        'active_table' => NULL,
        'cache_data' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_help().
 */
function cache_inspector_help($path, $arg) {
  switch ($path) {
    // Main module help.
    case 'admin/help#cache_inspector':
      return '<p>' . t('Got to the <a href="@url">Cache inspector</a> page,
      to see all unserialized cache values of all drupal cache tables.',
      array('@url' => url('admin/config/development/cache-inspector'))) . '</p>';
  }
}

/**
 * Implements hook_permission().
 */
function cache_inspector_permission() {
  return array(
    'access cache inspector' => array(
      'title' => t('Access Cache inspector'),
      'description' => t('Access serialized values in the drupal cache tables.'),
    ),
  );
}

/**
 * @} End of "defgroup drupal_hooks".
 */

/**
 * Show the cache data.
 */
function cache_inspector_view() {

  if (empty($_SESSION['cache_inspector'])) {
    $active_table = 'cache';
  }
  else {
    $active_table = $_SESSION['cache_inspector'];
  }

  $build = array();

  $build['#attached'] = array(
    'css' => array(drupal_get_path('module', 'cache_inspector') . '/cache_inspector.css'),
  );

  $result = db_select($active_table, 'c')
    ->extend('PagerDefault')
    ->limit(50)
    ->fields('c', array('cid', 'data'))
    ->execute()
    ->fetchAllAssoc('cid');

  $cache_data = array();
  if (!empty($result)) {
    foreach ($result as $cid => $data) {
      $cache_data[$cid . 'data'] = array(
        '#theme' => 'cache_inspector_data',
        '#cid' => $cid,
        // @unserialize: To avoid notice erros:
        // Notice: unserialize(): Error at offset 0 of x bytes
        '#data' => kdevel_print_object(@unserialize($data->data)),
      );
    }
    $cache_data['pager'] = array(
      '#markup' => theme('pager'),
    );
  }
  else {
    $cache_data['empty'] = array(
      '#markup' => t('Cache table is empty.'),
    );
  }

  $build['cache_inspector_page'] = array(
    '#theme' => 'cache_inspector_page',
    '#form' => drupal_get_form('cache_inspector_form'),
    '#active_table' => $active_table,
    '#cache_data' => $cache_data,
  );

  return $build;
}

/**
 * Theme function of the whole cache inspector page.
 */
function theme_cache_inspector_page($variables) {
  $form = $variables['form'];
  $active_table = $variables['active_table'];
  $cache_data = $variables['cache_data'];

  return render($form) . '<h2>' . $active_table . ' <em> (cache table)</em></h2><hr>' . render($cache_data);
}

/**
 * Theme function for one cache data.
 */
function theme_cache_inspector_data($variables) {
  $cid = $variables['cid'];
  $data = $variables['data'];

  return '<p>' . $cid . ' <em>(cid)</em></p>' . $data;
}

/**
 * Form to select the wanted cache table.
 */
function cache_inspector_form($form, &$form_state) {

  foreach (array_keys(drupal_get_schema()) as $table) {
    if (substr($table, 0, 5) == 'cache') {
      $options[$table] = $table;
    }
  }

  $form['table'] = array(
    '#type' => 'select',
    '#title' => t('Select cache table'),
    '#options' => $options,
    '#default_value' => empty($_SESSION['cache_inspector']) ? 'cache' : $_SESSION['cache_inspector'],
    '#required' => TRUE,
    '#weight' => 1,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('select'),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Process cache_inspector_form form submissions.
 */
function cache_inspector_form_submit($form, &$form_state) {
  $_SESSION['cache_inspector'] = $form_state['values']['table'];
}
