
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Cache inspector is a small development module. It can take a look in all
drupal cache tables. It requires the devel module. It uses the krumo framework
(http://krumo.sourceforge.net/) to visualize the cache data.

The cache inspector module helps a lot, if you want study the drupal cache
system or implement your own cache bins.

INSTALLATION
------------

1) Download the package from
   https://www.drupal.org/project/cache_inspector

3) Upload the cache_inspector files to the modules directory.

4) Go to admin/modules and enable the needed modules from the Development group.

You should end up with a structure like:

   /drupal/sites/all/modules/cache_inspector/cache_inspector.info
   /drupal/sites/all/modules/cache_inspector/cache_inspector.module
   ...

MAINTAINERS
-----------

Current maintainers:
 * Josef Friedrich (joseffriedrich) - https://drupal.org/user/580678
